#!/usr/bin/env python3
import random
import math
import matplotlib.pyplot as plt

def generate_poisson(lam):
    x = 0
    p = math.exp(-lam)
    u = random.random()
    while u > p:
        x += 1
        p += math.exp(-lam) * (lam**x) / math.factorial(x)
    return x


# say hello


requestAmount = 100000
curLambda = 0.1

arguments = []
averageMessagesInWindow = []
averageMessagesInQueue = []

for _ in range(9):
    sendedMessages = []
    messagesQueue = []
    generatedMessages = []
    
    for i in range(requestAmount):
        for id in range(len(messagesQueue)):
            messagesQueue[id] = messagesQueue[id] + 1
        
        if (len(messagesQueue) > 0):
            temp = messagesQueue.pop(0)
            sendedMessages.append(temp)

        sendersAmount = generate_poisson(curLambda)

        tempMes = [random.random() for _ in range(sendersAmount)] # sync
        #tempMes = [0 for _ in range(sendersAmount)] #async

        messagesQueue = messagesQueue + tempMes
        generatedMessages.append(len(messagesQueue))
    
    averageMessagesInQueue.append(sum(generatedMessages) / len(generatedMessages))
    averageMessagesInWindow.append(sum(sendedMessages) / len(sendedMessages))
    
    arguments.append(curLambda)
    curLambda = curLambda + 0.1


valuesTheory= []
for p in arguments:
    # n = ( (2 - p)) / (2 * (1 - p)) + 0.5 #sync
    n = ( (2 - p)) / (2 * (1 - p)) #async
    valuesTheory.append(n)
    
plt.plot(arguments, averageMessagesInWindow, label='Синхронная (практ)')
plt.plot(arguments, valuesTheory, label='Синхронная (теор)')
plt.legend()
plt.title('Средняя задержка')
plt.xlabel('lambda')
plt.ylabel('d(lambda)')
plt.show()

valuesTheory= []
for p in arguments:
    n = ( (2 - p) * p) / (2 * (1 - p))
    valuesTheory.append(n)
plt.plot(arguments, averageMessagesInQueue, label='практ')
plt.plot(arguments, valuesTheory, label='теор')
plt.legend()
plt.title('Среднее количество пользователей')
plt.xlabel('lambda')
plt.ylabel('N')
plt.show()
